from linear import Linear
import dotenv
import os

if __name__ == '__main__':
    dotenv.load_dotenv()
    L = Linear(os.getenv('LINEAR_API_KEY'))
    projects = L.projects()
    print(L.states(), L.teams(), projects)

    ret = L.create_issue('test', 'test123', project_id='fda4589c-08d1-4e88-8e1f-c18ed71a72d9', state_id='b6ca172f-3e9e-4a4a-8d72-d2d0dbc1504d', team_id='1ca59f5c-4136-4b8f-b33f-bee3f227d919')

    print(ret)